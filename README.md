## Setup
You can use the "death" delegate (event dispatcher) to assign functions or events that trigger when the character dies.

Additionally, in the class defaults, you can tweak the settings for if you want the character to have fall damage or not.

## Current Version
`Unreal Engine 4.19`
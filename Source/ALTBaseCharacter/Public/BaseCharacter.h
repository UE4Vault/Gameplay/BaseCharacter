// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/DamageType.h"
#include "Engine/World.h"
#include "BaseCharacter.generated.h"

class UCharacterMovementComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeath);

DECLARE_LOG_CATEGORY_EXTERN(LogBaseCharacter, Log, All);

UCLASS()
class ALTBASECHARACTER_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseCharacter();


	// Functions


	UFUNCTION(BlueprintCallable, Category = "Health")
		void UpdateHealth(float Value);

	UFUNCTION(BlueprintCallable, Category = "Health")
		void OnDeath();

	UFUNCTION(BlueprintPure, Category = "Health")
		bool IsDead();


	// Properties


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
		float MaxHealth = 100;

	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float Health;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Health")
		FDeath Death;

	//Determines whether we will apply fall damage to this character or not
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fall Damage")
		bool  bFallDamage = true;

	//Can gravity affect the dall damage applies to this character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fall Damage")
		bool bGravityCanAffect = true;

	UPROPERTY(Editanywhere, BlueprintReadWrite, Category = "Fall Damage")
		bool bScaleWithMaxHealth = false;

	//The start height for calculating fall damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fall Damage")
		float StartHeight = 1500;

	//The multiplier to determine how much we damage the player when they fall
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Fall Damage")
		float Multiplier = 1;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Called when the character lands on the ground
	virtual void Landed(const FHitResult &Hit) override;

	//Called when actor is created
	virtual void OnConstruction(const FTransform& Transform);
};
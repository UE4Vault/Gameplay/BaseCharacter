// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"

DEFINE_LOG_CATEGORY(LogBaseCharacter);

// Sets default values
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Health = MaxHealth;
}


void ABaseCharacter::UpdateHealth(float Value)
{
	Health = Value + Health;
	//Dead
	if (Health <= 0)
	{
		Death.Broadcast();
		Health = 0;
		//Remove Status Effects
		//Save Health
		DisableInput(Cast<APlayerController>(GetController()));
	}
	//Alive
	else
	{
		if (Health > MaxHealth)
			Health = MaxHealth;
		//Save Health
	}
}

void ABaseCharacter::OnDeath()
{
	UE_LOG(LogBaseCharacter, Warning, TEXT("Death"));
}

bool ABaseCharacter::IsDead()
{
	return Health <= 0;
}

// Called when the game starts or when spawned
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	Death.AddDynamic(this, &ABaseCharacter::OnDeath);
}

// Called every frame
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABaseCharacter::Landed(const FHitResult &Hit)
{
	Super::Landed(Hit);

	if (bFallDamage)
	{
		//Dilute the velocity to prepare to convert it to damage
		float DilutedVelocity = GetCharacterMovement()->Velocity.Z / 100;
		//How severe the the damage will be
		float Severity = UKismetMathLibrary::Abs(GetCharacterMovement()->Velocity.Z / 1000);
		//The resulting damage after taking the diluted velocity, multiplier, and severity into account.
		float Damage = DilutedVelocity * Multiplier * Severity;

		//Scale with Gravity?
		Damage = bGravityCanAffect ? (Damage * GetCharacterMovement()->GravityScale) : Damage;

		//Scale with Max Health?
		Damage = bScaleWithMaxHealth ? (Damage * (MaxHealth / 100)) : Damage;

		//Make sure the character is falling. Also make sure that they passed the StartHeight for when we start calculating for fall damage.
		if (GetCharacterMovement()->Velocity.Z < 0 && UKismetMathLibrary::Abs(GetCharacterMovement()->Velocity.Z) > StartHeight)
		{
			UGameplayStatics::ApplyDamage(this, Damage, this->Controller, this, UDamageType::StaticClass());
			UE_LOG(LogBaseCharacter, Log, TEXT("Velocity %f"), GetCharacterMovement()->Velocity.Z);
		}
	}
}

// Called when actor constructs
void ABaseCharacter::OnConstruction(const FTransform& Transform)
{
	Health = MaxHealth;
}